﻿// Define your product name and version
tomtom.setProductInfo('Simee', '1.0');
tomtom.key('E475uHfdbwuGbjxNFXyxXT3zvtiya9PG');

var markerOptions = {
    icon: tomtom.L.icon({
        iconUrl: 'https://i.ibb.co/cKqcy54/fork-and-knife-with-plate.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_2 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/runner_1f3c3.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_3 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/skier_26f7.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_4 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/223/sleeping-symbol_1f4a4.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_5 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/automobile_1f697.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_6 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/guitar_1f3b8.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_7 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/223/slot-machine_1f3b0.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};
var markerOptions_8 = {
    icon: tomtom.L.icon({
        iconUrl: 'https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/apple/237/soccer-ball_26bd.png',
        iconSize: [75, 80],
        iconAnchor: [15, 34]
    })
};

var map = tomtom.map('map', {
    key: 'E475uHfdbwuGbjxNFXyxXT3zvtiya9PG',
    source: ['vector', 'raster'],
    basePath: '~/lib/tomtomsdk',
    vector: {
        fallback: 'raster' //in case WebGL is not supported in browser
    },
    center: [39.545551, -119.816668],
    zoom: 14.4
});

tomtom.L.marker([39.544596, -119.816108], markerOptions).addTo(map);
tomtom.L.marker([39.543643, -119.817707], markerOptions_2).addTo(map);
tomtom.L.marker([39.327149, -119.885203], markerOptions_3).addTo(map);
tomtom.L.marker([39.503191, -119.805028], markerOptions_4).addTo(map);
tomtom.L.marker([39.520892, -119.868356], markerOptions_5).addTo(map);
tomtom.L.marker([39.524354, -119.811189], markerOptions_6).addTo(map);
tomtom.L.marker([39.524419, -119.781069], markerOptions_7).addTo(map);
tomtom.L.marker([39.547621, -119.815249], markerOptions_8).addTo(map);


var languageLabel = L.DomUtil.create('label');
languageLabel.innerHTML = 'Map language';
var languageSelector = tomtom.languageSelector.getHtmlElement(tomtom.globalLocaleService, 'maps');
languageLabel.appendChild(languageSelector);
tomtom.controlPanel({
    position: 'bottomright',
    title: 'Settings',
    collapsed: true,
    closeOnMapClick: false
})
    .addTo(map)
    .addContent(languageLabel);
tomtom.controlPanel({
    position: 'topright',
    title: null,
    collapsed: false,
    closeOnMapClick: false,
    close: null,
    show: null
})
    .addTo(map)
    .addContent(document.getElementById('map').nextElementSibling);
function updateBaseLayer() {
    var selectedOption = document.getElementById('baseLayer').value || 'raster';
    var baseLayers = map.getBaseLayers();
    if (selectedOption === 'raster') {
        map.addLayer(baseLayers.raster);
        map.removeLayer(baseLayers.vector);
    } else if (selectedOption === 'vector') {
        map.addLayer(baseLayers.vector);
        map.removeLayer(baseLayers.raster);
    }
}
function updateLayersOptions(key) {
    return function () {
        var option = {};
        option[key] = this.value;
        map.updateLayersOptions(option);
    };
}
document.getElementById('map_style').onchange = updateLayersOptions('style');
document.getElementById('map_layer').onchange = updateLayersOptions('layer');
document.getElementById('baseLayer').onchange = updateBaseLayer();
(function initializeTileSwitcher() {
    var select = document.getElementById('baseLayer');
    var layers = map.getBaseLayers();
    function newOption(value, label, selected) {
        var option = document.createElement('option');
        option.value = value;
        option.text = label;
        if (selected) {
            option.selected = 'selected';
        }
        return option;
    }
    layers.raster && select.appendChild(newOption('raster', 'Raster'));
    layers.vector && select.appendChild(newOption('vector', 'Vector', 'selected'));
})();