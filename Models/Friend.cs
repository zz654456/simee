﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Simee.Models
{
    public class Friend
    {
        [Key]
        [Required(ErrorMessage = "User ID is required; [Required] attribute")]
        [RegularExpression(@"^N\d{8}$", ErrorMessage =
            "UserID must have the format Nxxxxxxxx x is any number from 0~9")]
        [Display(Name = "User ID:* ", Prompt = "Nxxxxxxxx ", Description = "Unique User Identifier")]
        //[Display(Name = "Contact ID:  ")]
        public string UserID { get; set; }

        // The LastName property is required and has a length less than 25.
        // Empty strings are not allowed.
        [Required(ErrorMessage = "UserName is required; [Required] attribute", AllowEmptyStrings = false)]
        [StringLength(25, ErrorMessage = "String Length must be between 1 and 25 ; [StringLength] attribute")]
        [Display(Name = "User Name* ", Prompt = "Enter User Name ", Description = "User Name")]
        public string UserName { get; set; }
    }
}
