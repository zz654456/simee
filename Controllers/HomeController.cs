﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Simee.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;


namespace Simee.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        
        string ucode;
        string behavior;


        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View("LoginView_2");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult LoginView()
        {
            return View();
        }

        public IActionResult LoginView_2()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View("Index");
        }

        [HttpPost]
        public IActionResult SearchEmoji(/*string Emojikeyword*/)
        {

            string Keyword = Request.Form["Emojikeyword"];

            //string Keyword = "soccer";
            string emoji_list = System.IO.File.ReadAllText("wwwroot/json/emoji.json");




            /* **** For emoji.json **** */

            JArray array = JsonConvert.DeserializeObject<JArray>(emoji_list.ToString());


            //string v1 = obj.Value<string>("short_name");/*取得該CategoryName的值*/
            foreach (JObject Jobj in array)
            {
                if (Jobj["short_name"].ToString() == Keyword)
                {
                    ucode = Jobj["unified"].ToString();
                }
                else
                    Console.WriteLine("{0} is not {1}.", Jobj["short_name"].ToString(), Keyword);
            }

            ViewData["emoji_ex1"] = "&#x" + ucode;


            return View("Index_2");

        }

        [HttpPost]
        public IActionResult EditMeasurement()
        {

            string heartrate = Request.Form["Editheartrate"];
            int int_heartrate = Int32.Parse(heartrate);
            

            //string Keyword = "soccer";
            string behavior_list = System.IO.File.ReadAllText("wwwroot/json/Match-Behav-Measure.json");




            /* **** For emoji.json **** */

            JArray array = JsonConvert.DeserializeObject<JArray>(behavior_list.ToString());


            //string v1 = obj.Value<string>("short_name");/*取得該CategoryName的值*/
            foreach (JObject Jobj in array)
            {
                string low_heart_rate = Jobj["low-heart-rate"].ToString();
                int int_low_heart_rate = Int32.Parse(low_heart_rate);
                string high_heart_rate = Jobj["high-heart-rate"].ToString();
                int int_high_heart_rate = Int32.Parse(high_heart_rate);
                if (int_heartrate <= int_high_heart_rate && int_heartrate >= int_low_heart_rate)
                {
                    behavior = Jobj["behavior"].ToString();
                }
                
            }

            ViewData["emoji_ex2"] = behavior;


            return View("Index_2");

        }







        public IActionResult ViewModelDemo()
        {
            ViewBag.Title = "ViewModel传值示例";
            var person = new Emoji
            {
                image = "0023-fe0f-20e3.png",
                name = "soccer"
            };
            //等同于 return View("ViewModelDemo", person);
            return View(person);
        }

        [HttpGet]
        public IActionResult AddFriend()
        {
            return View();
        }

        // POST: Home/AddFriend
        //
        // Second part of the Add operation. The form is posted to this
        // action method. Form fields are bound using the [Bind] attribute. Form
        // fields are passed in student.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddFriend([Bind("UserID,UserName")]
            Friend friend)
        {

            // If the model is not valid, return the Create view. This is the
            // default view.
            return View("Index_2");
        }

        public IActionResult Index_2()
        {
            return View();
        }

        [HttpGet]
        public IActionResult UserFile()
        {
            return View();
        }


        public string BehaviorConjectured()
        {
            var result = "running";

            return result;
        }

    }
}
